package glavni;

import algoritmi.GradijentniSpust;
import algoritmi.NewtonRaphson;
import funkcije.DerivableFunction;
import funkcije.FunkcijaF1;
import funkcije.FunkcijaF2;
import point.IPoint;
import point.Point;

public class DrugiZadatak {
	
	public static void main(String[] args) {
		System.out.println("Drugi zadatak");
		
		DerivableFunction f1 = new FunkcijaF1();
		DerivableFunction f2 = new FunkcijaF2();
		IPoint pocetna1 = Point.ParsePoint("-1.9 2");
		IPoint pocetna2 = Point.ParsePoint("0.1 0.3");
		
		System.out.println("F1 (banana)");
		GradijentniSpust.PostupakGradijetnogSpusta(f1, pocetna1, true);
		System.out.println();	
		f1 = new FunkcijaF1();
		IPoint nr1 = NewtonRaphson.NewtonRaphsoinovPostupak(f1, pocetna1, true);
		nr1.print();
		System.out.println(f1.getValueAtPoint(nr1));
		System.out.println();
		System.out.println("F2");
		GradijentniSpust.PostupakGradijetnogSpusta(f2, pocetna2, true);
		System.out.println();
		f2 = new FunkcijaF2();
		IPoint nr2 = NewtonRaphson.NewtonRaphsoinovPostupak(f2, pocetna2, true);
		nr2.print();
		System.out.println(f2.getValueAtPoint(nr2));
		System.out.println();
		System.out.println("Napomena: ako se poveca dopusteni broj iteracija bez poboljsanja za gradijentni spust, u konacnici se ipak dobije dobro rjesenje za f1.");
		System.out.println();
	}

}
