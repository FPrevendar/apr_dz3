package glavni;

import algoritmi.TransformacijaUProblemBezOgranicenja;
import algoritmi.TransformacijaUProblemBezOgranicenja.PostupakZaPrimjenu;
import funkcije.FunkcijaF1;
import funkcije.FunkcijaF2;
import funkcije.IFunction;
import ogranicenja.OgranicenjeJednakosti;
import ogranicenja.OgranicenjeNejednakosti;
import point.IPoint;
import point.Point;

public class CetvrtiZadatak {
	
	public static void main(String[] args) {
		System.out.println("Cetvrti Zadatak");
		System.out.println("Transformacija u mjesoviti oblik");
		IFunction f1 = new FunkcijaF1();
		IPoint f1pocetna = Point.ParsePoint("-1.9 2");
		IFunction f2 = new FunkcijaF2();
		IPoint f2pocetna = Point.ParsePoint("0.1 0.3");
		
		System.out.println("Ogranicenja su:");
		OgranicenjeNejednakosti n1= new OgranicenjeNejednakosti(2) {
			
			@Override
			public double vrijednostOgranicenja(IPoint tocka) {
				return tocka.get(1) - tocka.get(0);
			}
		};
		System.out.println("x2 - x1 >= 0");
		
		OgranicenjeNejednakosti n2 = new OgranicenjeNejednakosti(2) {
			
			@Override
			public double vrijednostOgranicenja(IPoint tocka) {
				return 2 - tocka.get(0);
			}
		};
		System.out.println("2 - x1 >= 0");
		System.out.println();
		
		OgranicenjeNejednakosti[] nejed = {n1, n2};
		OgranicenjeJednakosti[] jed = new OgranicenjeJednakosti[0];
		
		System.out.println("F1 (banana)");
		System.out.println("Hooke-Jeeves");
		IPoint f1hj = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f1, f1pocetna, jed, nejed, PostupakZaPrimjenu.HOOKE_JEEVES);
		f1hj.print();
		System.out.println(f1.getValueAtPoint(f1hj));
		System.out.println("Nelder Mead");
		IPoint f1nm = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f1, f1pocetna, jed, nejed, PostupakZaPrimjenu.SIMPLEKS_NELDER_MEAD);
		f1nm.print();
		System.out.println(f1.getValueAtPoint(f1nm));
		System.out.println();
		System.out.println("F2");
		System.out.println("Hooke-Jeeves");
		IPoint f2hj = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f2, f2pocetna, jed, nejed, PostupakZaPrimjenu.HOOKE_JEEVES);
		f2hj.print();
		System.out.println(f2.getValueAtPoint(f2hj));
		System.out.println("Nelder Mead");
		IPoint f2nm = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f2, f2pocetna, jed, nejed, PostupakZaPrimjenu.SIMPLEKS_NELDER_MEAD);
		f2nm.print();
		System.out.println(f2.getValueAtPoint(f2nm));
		System.out.println();

	}

}
