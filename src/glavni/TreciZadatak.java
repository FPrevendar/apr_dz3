package glavni;

import algoritmi.Box;
import funkcije.*;
import ogranicenja.EksplicitnaOgranicenjaZaSveVarijable;
import ogranicenja.OgranicenjeNejednakosti;
import point.IPoint;
import point.Point;

public class TreciZadatak {

	public static void main(String[] args) {
		System.out.println("Treci zadatak");
		System.out.println("Boxov postupak");		
		System.out.println("Ogranicenja su:");
		OgranicenjeNejednakosti o1 = new OgranicenjeNejednakosti(2) {
			
			@Override
			public double vrijednostOgranicenja(IPoint tocka) {
				return tocka.get(1) - tocka.get(0);
			}
		};
		System.out.println("x2-x1 >= 0");
		
		OgranicenjeNejednakosti o2 = new OgranicenjeNejednakosti(2) {
			
			@Override
			public double vrijednostOgranicenja(IPoint tocka) {
				return 2 - tocka.get(0);
			}
		};
		System.out.println("2-x1 >= 0");
		
		OgranicenjeNejednakosti[] ogranicenjaNejednakosti = {o1, o2};
		
		EksplicitnaOgranicenjaZaSveVarijable eks = new  EksplicitnaOgranicenjaZaSveVarijable(
				Point.i(2).mul(-100),
				Point.i(2).mul(100));
		
		System.out.println("xi E [-100, 100]");
		
		IFunction f1 = new FunkcijaF1();
		IFunction f2 = new FunkcijaF2();
		IPoint f1pocetna = Point.ParsePoint("-1.9 2");
		IPoint f2pocetna = Point.ParsePoint("0.1 0.3");
		
		System.out.println("F1 (banana)");
		IPoint f1rj = Box.BoxovPostupak(f1, f1pocetna, eks, ogranicenjaNejednakosti);
		f1rj.print();
		System.out.println(f1.getValueAtPoint(f1rj));
		System.out.println("F2");
		IPoint f2rj = Box.BoxovPostupak(f2, f2pocetna, eks, ogranicenjaNejednakosti);
		f2rj.print();
		System.out.println(f2.getValueAtPoint(f2rj));
		System.out.println();
	}

}
