package glavni;

import algoritmi.TransformacijaUProblemBezOgranicenja;
import algoritmi.TransformacijaUProblemBezOgranicenja.PostupakZaPrimjenu;
import funkcije.FunkcijaF4;
import funkcije.IFunction;
import ogranicenja.OgranicenjeJednakosti;
import ogranicenja.OgranicenjeNejednakosti;
import point.IPoint;
import point.Point;

public class PetiZadatak {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Peti Zadatak");
		IFunction f4 = new FunkcijaF4();
		
		IPoint pocetna = Point.ParsePoint("0 0");
		IPoint pocetna2 = Point.ParsePoint("5 5");
		
		System.out.println("Ogranicenja su:");
		OgranicenjeNejednakosti n1 = new OgranicenjeNejednakosti(2) {
			
			@Override
			public double vrijednostOgranicenja(IPoint tocka) {
				return 3 - tocka.get(0) - tocka.get(1);
			}
		};
		System.out.println("3 - x1 - x2 >= 0");
		
		OgranicenjeNejednakosti n2 = new OgranicenjeNejednakosti(2) {
			
			@Override
			public double vrijednostOgranicenja(IPoint tocka) {
				return 3 + 1.5 * tocka.get(0) - tocka.get(1);
			}
		};
		System.out.println("3 + 1.5 * x1 - x2 >= 0");
		
		OgranicenjeJednakosti j1 = new OgranicenjeJednakosti(2, 10e-6) {
			
			@Override
			public double vrijednostOgranicenja(IPoint tocka) {
				return tocka.get(1) - 1;
			}
		};
		System.out.println("x2 - 1 = 0");
		
		OgranicenjeNejednakosti[] nejed = {n1, n2};
		OgranicenjeJednakosti[] jed = {j1};
		
		System.out.println("F4");
		System.out.println("Pocetna tocka (0, 0)");
		System.out.println("Hooke Jeeves");
		IPoint hj1 = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f4, pocetna, jed, nejed, PostupakZaPrimjenu.HOOKE_JEEVES);
		hj1.print();
		System.out.println(f4.getValueAtPoint(hj1));
		System.out.println("Nelder Mead");
		IPoint nm1 = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f4, pocetna, jed, nejed, PostupakZaPrimjenu.SIMPLEKS_NELDER_MEAD);
		nm1.print();
		System.out.println(f4.getValueAtPoint(nm1));
		System.out.println();
		System.out.println("Pocetna tocka (5, 5)");
		System.out.println("Hooke Jeeves");
		IPoint hj2 = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f4, pocetna2, jed, nejed, PostupakZaPrimjenu.HOOKE_JEEVES);
		hj2.print();
		System.out.println(f4.getValueAtPoint(hj2));
		System.out.println("Nelder Mead");
		IPoint nm2 = TransformacijaUProblemBezOgranicenja.PostupakTransformacijeUProblemBezOgranicenja(f4, pocetna2, jed, nejed, PostupakZaPrimjenu.SIMPLEKS_NELDER_MEAD);
		nm2.print();
		System.out.println(f4.getValueAtPoint(nm2));

	}

}
