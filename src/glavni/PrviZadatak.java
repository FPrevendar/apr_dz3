package glavni;

import algoritmi.GradijentniSpust;
import funkcije.DerivableFunction;
import funkcije.FunkcijaF3;
import point.IPoint;
import point.Point;

public class PrviZadatak {

	public static void main(String[] args) {
		System.out.println("Prvi zadatak");
		System.out.println("F3");
		DerivableFunction funkcija = new FunkcijaF3();
		
		IPoint pocetnaTocka = Point.ParsePoint("0.1 0.3");
		
		GradijentniSpust.PostupakGradijetnogSpusta(funkcija, pocetnaTocka, true);
		
		funkcija = new FunkcijaF3();
		System.out.println();
		GradijentniSpust.PostupakGradijetnogSpusta(funkcija, pocetnaTocka, false);
		
		System.out.println("Uz trazenje optimalnog pomaka postupak konvergira, a inace divergira.");
		System.out.println();
	}

}
