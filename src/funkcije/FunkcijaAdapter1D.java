package funkcije;

import matrix.IMatrix;
import point.IPoint;
import point.Point;

public class FunkcijaAdapter1D extends AbstractFunction {
	
	private IFunction originalnaFunkcija;
	private IPoint pocetnaTocka;
	private IPoint smjer;
	
	public FunkcijaAdapter1D(IFunction originalnaFunkcija,
			IPoint pocetnaTocka,
			IMatrix smjer) {
		this.originalnaFunkcija = originalnaFunkcija;
		this.pocetnaTocka = pocetnaTocka.copy();
		IPoint p = new Point(smjer.visina());
		for(int i = 0; i < smjer.visina(); i++) {
			p.set(i, smjer.get(i, 0));
		}
		this.smjer = p;
	}

	@Override
	protected double getValue(IPoint point) {
		double lambda = point.get(0);
		return originalnaFunkcija.getValueAtPoint(pocetnaTocka.add(smjer.mul(lambda)));
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == 1) {
			return true;
		}
		return false;
	}

}
