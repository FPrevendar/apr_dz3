package funkcije;

import matrix.IMatrix;
import matrix.Matrix;
import point.IPoint;

public abstract class AbstractDerivableFunction extends AbstractFunction implements DerivableFunction{

	protected IFunction[] derivacije;
	protected IFunction[][] drugeDerivacije;
	private int brojRacunanjaGradijeta = 0;
	private int brojRacunanjaHessiana = 0;

	@Override
	public IMatrix racunajGradijent(IPoint tocka) {
		brojRacunanjaGradijeta = getBrojRacunanjaGradijeta() + 1;
		IMatrix m = new Matrix(tocka.getDimenstion(), 1);
		for(int i = 0; i < tocka.getDimenstion(); i++) {
			m.set(i, 0, derivacije[i].getValueAtPoint(tocka));
		}
		return m;
	}

	@Override
	public int getBrojRacunanjaGradijeta() {
		return brojRacunanjaGradijeta;
	}

	@Override
	public IMatrix racunajHessiana(IPoint tocka) {
		brojRacunanjaHessiana = getBrojRacunanjaHessiana() + 1;
		IMatrix m = new Matrix(tocka.getDimenstion(), tocka.getDimenstion());
		for(int i = 0; i < tocka.getDimenstion(); i++){
			for(int j = 0; j < tocka.getDimenstion(); j++){
				m.set(i, j, drugeDerivacije[i][j].getValueAtPoint(tocka));
			}
		}
		return m;
	}

	@Override
	public int getBrojRacunanjaHessiana() {
		return brojRacunanjaHessiana;
	}

}