package funkcije;

import point.IPoint;

public class FunkcijaBroj2 extends AbstractFunction{

	@Override
	protected double getValue(IPoint point) {
		double x1 = point.get(0);
		double x2 = point.get(1);
		double fx = Math.pow(x1 - 4, 2) + 4 * Math.pow(x2 - 2, 2);
		return fx;
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == 2) {
			return true;
		}
		return false;
	}
	
}
