package funkcije;

import point.IPoint;

public class FunkcijaF2 extends AbstractDerivableFunction {
	
	public FunkcijaF2() {
		derivacije = new IFunction[2];
		derivacije[0] = new dfdx1();
		derivacije[1] = new dfdx2();
		
		drugeDerivacije = new IFunction[2][2];
		drugeDerivacije[0][0] = new d2fdx12();
		drugeDerivacije[0][1] = new d2fdx1dx2();
		drugeDerivacije[1][0] = new d2fdx1dx2();
		drugeDerivacije[1][1] = new d2fdx22();
		drugeDerivacije[1][0] = new d2fdx1dx2();
	}

	@Override
	protected double getValue(IPoint point) {
		double x1 = point.get(0);
		double x2 = point.get(1);
		return Math.pow(x1 - 4, 2) + 4 * Math.pow(x2 - 2, 2);
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == 2) {
			return true;
		}
		return false;
	}
	
	private class dfdx1 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			double x1 = point.get(0);
			return 2 * x1 - 8;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class dfdx2 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			double x2 = point.get(1);
			return 8 * x2 - 16;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class d2fdx12 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			return 2;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class d2fdx22 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			return 8;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class d2fdx1dx2 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			return 0;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
}
