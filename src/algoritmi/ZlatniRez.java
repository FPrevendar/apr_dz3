package algoritmi;

import funkcije.IFunction;
import point.IPoint;

public class ZlatniRez {
	
	private static boolean ispis = false;

	public static void setIspis(boolean ispis) {
		ZlatniRez.ispis = ispis;
	}

	public static Interval PostupakZlatnogReza(
			IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			Interval pocetniInterval,
			int koordinata,
			double preciznost) {
		Interval interval = new Interval();
		if(pocetnaTocka.getDimenstion() <= koordinata) {
			throw new IllegalArgumentException("Kriva dimenzija u postupku zlatnog reza");
		}
		if(ispis) {
			System.out.println("Pokrecem postupak zlatnog reza");
			System.out.printf("Interval koji smanjujem je: %f %f\n", pocetniInterval.left, pocetniInterval.right);
		}
		
		IPoint tocka = pocetnaTocka.copy();
		double k = 0.5 * (Math.sqrt(5) - 1);
		double a = pocetniInterval.left;
		double b = pocetniInterval.right;
		double c = b - k * (b - a);
		double d = a + k * (b - a);
		
		tocka.set(koordinata, c);
		double fc = funkcijaCilja.getValueAtPoint(tocka);
		
		tocka.set(koordinata, d);
		double fd = funkcijaCilja.getValueAtPoint(tocka);
		if(ispis) {
			System.out.println("    a    |     c   |    d    |    b    ||   f(c)  |   f(d)  |f(c) < f(d)");
		}
		while((b - a) > preciznost) {
			if(ispis) {
				System.out.printf("% 7f|% 7f|% 7f|% 7f||% 7f|% 7f|%b\n", a, c, d, b, fc, fd, fc < fd);
			}
			if(fc < fd) {
				b = d;
				d = c;
				c = b - k * (b - a);
				fd = fc;
				
				tocka.set(koordinata, c);
				fc = funkcijaCilja.getValueAtPoint(tocka);	
			}else {
				a = c;
				c = d;
				d = a + k * (b - a);
				fc = fd;
				
				tocka.set(koordinata, d);
				fd = funkcijaCilja.getValueAtPoint(tocka);
			}
		}
		interval.left = a;
		interval.right = b;
		interval.tocka = tocka;
		return interval;
	}
	
	public static Interval PostupakZlatnogReza(
			IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			int koordinata,
			double preciznost,
			double korak) {
		Interval unimodalni = UnimodalniInterval.NadjiUnimodalniInterval(funkcijaCilja, pocetnaTocka, korak, koordinata);
		return PostupakZlatnogReza(funkcijaCilja, pocetnaTocka, unimodalni, koordinata, preciznost);
	}
	
	public static Interval PostupakZlatnogReza(IFunction funkcijaCilja, IPoint pocetnaTocka, int koordinata) {
		return PostupakZlatnogReza(funkcijaCilja, pocetnaTocka, koordinata, 10e-6, 1);
	}
	
	public static Interval postupakZlatnogReza(IFunction funkcijaCilja, IPoint pocetnaTocka) {
		return PostupakZlatnogReza(funkcijaCilja, pocetnaTocka, 0);
	}
	
}
