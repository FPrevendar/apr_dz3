package ogranicenja;

import point.IPoint;

public class EksplicitnoOgranicenje implements IOgranicenje{
	
	private int brojDimenzija;
	private int dimenzija;
	private double min;
	private double max;
	
	public EksplicitnoOgranicenje(int brojDimenzija, int dimenzija, double min, double max) {
		if(min > max) {
			throw new IllegalArgumentException("Min vrijednost ne moze biti veca od max.");
		}
		this.brojDimenzija = brojDimenzija;
		this.dimenzija = dimenzija;
		this.min = min;
		this.max = max;
	}
	
	@Override
	public boolean jeLiZadovoljeno(IPoint tocka) {
		if(!checkDimension(tocka)) {
			throw new IllegalArgumentException("Pogresna velicina tocke");
		}
		if(tocka.get(dimenzija) < max && tocka.get(dimenzija) > min) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean checkDimension(IPoint tocka) {
		if(tocka.getDimenstion() == brojDimenzija) {
			return true;
		}
		return false;
	}
}
