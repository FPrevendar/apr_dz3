package ogranicenja;

import point.IPoint;

public abstract class OgranicenjeJednakosti implements IOgranicenje {
	
	protected double eps = 10-6;
	private int dimenzija;
	
	public OgranicenjeJednakosti(int dimenzija, double eps) {
		this.dimenzija = dimenzija;
		this.eps = eps;
	}
	
	public abstract double vrijednostOgranicenja(IPoint tocka);

	@Override
	public boolean jeLiZadovoljeno(IPoint tocka) {
		if(Math.abs(vrijednostOgranicenja(tocka)) < eps) {
			return true;
		}
		return false;
	}

	@Override
	public boolean checkDimension(IPoint tocka) {
		if(tocka.getDimenstion() == dimenzija) {
			return true;
		}
		return false;
	}

}
