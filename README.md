## 3\. domaća zadaća iz analize i projektiranja računalom

U ovoj domaćoj zadaći je bilo potrebno ostvarititi nekoliko metoda optimizacije temljene na gradijentima i nekoliko metoda ograničene optimizacije, to su:
* Gradijentni spust
* Newton-Raphsonov postupak
* Box-ov postupak optimizacije problema s ograničenjima
* Postupak pretvorbe problema s ograničenjima u problem bez ograničenja na mješoviti način

To je ostvareno u programskom jeziku java, i koriste se metode ostvarene u [1\.](https://gitlab.com/FPrevendar/apr_dz1) i [2\.](https://gitlab.com/FPrevendar/apr_dz2) domaćoj zadaći.

Tekst zadatka se nalazi [ovdje](https://www.fer.unizg.hr/_download/repository/vjezba_3%5B4%5D.pdf).
